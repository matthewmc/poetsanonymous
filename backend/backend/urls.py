from django.contrib import admin
from django.urls import path, include
from app_poems import views

urlpatterns = [
    path('', views.get_main_page),
    path('admin/', admin.site.urls),
    path('', include('django.contrib.auth.urls')),
]

# URL endpoints for getting poem json
urlpatterns += [path('poems/', 
                      include([path('post/', views.post_poem),
                               path('lit/<int:id_>/', views.get_lit_poem_page),
                               path('user/<int:id_>/', views.get_user_poem_page),]
                             )
                    )
               ]

urlpatterns += [path('reviews/', 
                      include([path('user/', views.post_user_poem_review),
                               path('lit/', views.post_lit_poem_review)])
                    )
               ]

urlpatterns += [path('profiles/<str:username>', views.get_user_profile),
               ]