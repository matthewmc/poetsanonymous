from django import forms

class ReviewForm(forms.Form):
    rating = forms.IntegerField()
    blind_vote = forms.BooleanField(required=False)
    poem_id = forms.IntegerField()

class PoemForm(forms.Form):
    title = forms.CharField()
    text = forms.CharField()