from django.apps import AppConfig


class AppPoemsConfig(AppConfig):
    name = 'app_poems'
