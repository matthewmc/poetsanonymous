from django.http import HttpResponse, JsonResponse, Http404, HttpResponseBadRequest
from django.shortcuts import render, get_object_or_404
from django.db.models import Avg
from app_poems.models import PubDomainPoem, UserPoem, ReviewOfPubDomPoem, ReviewOfUserPoem
from app_poems.forms import ReviewForm, PoemForm
from django.contrib.auth.models import User
from json import dumps
from datetime import date

# from django.views.decorators.http import require_safe

def get_main_page(request):
    context = {}
    return render(request, 'main_page.html', context)

def get_poem_json(model, id_):
    try: 
        res = model.objects.filter(id=id_)[0]
    except Exception as e:
        print('No such poem', e) 
        raise Http404('Failed db query')
    payload = {'title' : res.title,
               'text' : res.text}
    if model == UserPoem:
        payload['author'] = res.author.username
        payload['date'] = res.date
    elif model == PubDomainPoem:
        payload['author'] = res.author.name
    return payload 
    
# Takes poems id and review table name and returns poem and its ancillary data from the database.
def get_poem_ratings(table, id_):
    res = {'total_avg' : None,
           'blind_avg' : None,
          }

    # Get all poem reviews with matching poem id, then average the ratings.  
    try: 
        res['total_avg'] = table.objects.filter(poem_id=id_).aggregate(Avg('rating'))['rating__avg']
    except Exception as e:
        print('Failed to get or no poem ratings', e)
        raise Http404()
    # Get all poem reviews with matching poem id and blind_vote = True (i.e. users voted blind), then average the ratings.
    try: 
        res['blind_avg'] = table.objects.filter(poem_id=id_, blind_vote=True).aggregate(Avg('rating'))['rating__avg']
    except Exception as e:
        print('Failed to get or no poem ratings', e)
        raise Http404()
    return res

# Takes reviews table name, id, and the django request context for the reference to the session user, and returns
# user's past rating and whether they voted blind, if rating exists. 
def get_user_rating(request, table, id_):
    res = {'user_rating' : None,
           'rated_blind' : None,
          }

    if request.user.is_authenticated:
        try: 
            review = table.objects.filter(user=request.user, poem_id=id_)[0]
            res['user_rating'] = review.rating
            res['rated_blind'] = review.blind_vote
        except Exception as e:
            print(e)
    return res

# Closure for handling different models.  Takes poems and reviews table names to return view function.
def get_json(poem_table,review_table):
    def wrapped(request, id_):
        return JsonResponse({**get_poem_json(poem_table, id_),
                             **get_poem_ratings(review_table, id_),
                             **get_user_rating(request, review_table, id_)   
                            })
    return wrapped

# Views for poem and ancillary data json endpoints
get_lit_poem_page = get_json(PubDomainPoem, ReviewOfPubDomPoem)
get_user_poem_page = get_json(UserPoem, ReviewOfUserPoem)

# Closure to handle different review models/tables.
# Takes reviews table name. Returns view function for endpoint for POSTing reviews.
def post_review(review_table):
    def wrapped(request):
        form = ReviewForm(request.POST)
        if form.is_valid() and request.user.is_authenticated:
            data = form.cleaned_data
            review = {
                'poem_id' : data['poem_id'],
                'user' : request.user,
                'rating' : data['rating'],
                'blind_vote' : data['blind_vote']
            }

            # Validate review data.
            if review['rating'] not in range(1, 6):
                return HttpResponseBadRequest()

            # Check if user has already rated this poem and update to or insert new rating.
            res = review_table.objects.filter(poem_id=data['poem_id'], user=request.user)
            if len(res) != 0:
                # Use Django's "INSERT vs. UPDATE" system which if given existing pk, UPDATEs.  
                review['id'] = res[0].id 
                # Also, if the user has already rated cognizant of who the author is, the db should preserve
                # that.  
                if res[0].blind_vote == False:
                    review['blind_vote'] = False
            new_review = review_table(**review)
            new_review.save()
            return HttpResponse()
        else:
            return HttpResponseBadRequest()
    return wrapped

post_user_poem_review = post_review(ReviewOfUserPoem)
post_lit_poem_review = post_review(ReviewOfPubDomPoem)

def post_poem(request):
    if request.method == 'GET':
        form = PoemForm()
        return render(request, 'post_poem.html', {'form' : form})
    elif request.method == 'POST':
        form = PoemForm(request.POST)
        if form.is_valid() and request.user.is_authenticated:
            data = form.cleaned_data
            new_poem = {'author' : request.user,
                        'title' : data['title'],
                        'text' : data['text'],
                        'date' : date.today(),
                       }
            new_poem = UserPoem(**new_poem)
            new_poem.save()
            return HttpResponse()
        else:
            return HttpResponseBadRequest()

def get_user_profile(request, username):
    curr_user = User.objects.filter(username=username)[0]
    curr_user_poems = curr_user.userpoem_set.all()
    print(curr_user_poems)
    return render(request, 
                  'profile_page.html', 
                  {'username' : username,
                   'curr_user_poems' : curr_user_poems,
                  },
                 )