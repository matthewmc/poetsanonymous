from django.db import models
from django.contrib.auth.models import User

# Table for public domain authors with a column for a wikipedia or informative link.
class Author(models.Model):
    name = models.TextField(max_length=10000)
    info_url = models.TextField(max_length=10000)

# -----------------------------------------------------------------------

class Poem(models.Model):
    title = models.TextField(max_length=10000)
    text = models.TextField(max_length=10000)

    def __str__(self):
        return 'Title: {}, Text: {}'.format(str(self.title), str(self.text))

    class Meta: 
        abstract = True

class UserPoem(Poem):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField(default='3000-01-01')

class PubDomainPoem(Poem):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)

# -----------------------------------------------------------------------

class Review(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    rating = models.IntegerField()
    blind_vote = models.BooleanField()
    poem_id = models.IntegerField()

    def __str__(self):
        return 'Rating: {}, Voted blind: {}, Poem ID: {}'.format(self.rating,
                                                                 self.blind_vote,
                                                                 self.poem_id,
                                                                )

    class Meta: 
        unique_together = ('poem_id', 'user')
        abstract = True

class ReviewOfUserPoem(Review):
    pass

class ReviewOfPubDomPoem(Review):
    pass