# Generated by Django 2.0 on 2018-04-29 00:54

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app_poems', '0004_auto_20180428_1646'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='reviewofpubdompoem',
            unique_together={('poem_id', 'user')},
        ),
        migrations.AlterUniqueTogether(
            name='reviewofuserpoem',
            unique_together={('poem_id', 'user')},
        ),
    ]
