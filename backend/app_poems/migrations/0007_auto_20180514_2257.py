# Generated by Django 2.0.4 on 2018-05-15 05:57

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app_poems', '0006_auto_20180504_1915'),
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=10000)),
                ('info_url', models.TextField(max_length=10000)),
            ],
        ),
        migrations.AddField(
            model_name='userpoem',
            name='date',
            field=models.DateField(default='3000-01-01'),
        ),
        migrations.AlterField(
            model_name='pubdomainpoem',
            name='author',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_poems.Author'),
        ),
        migrations.AlterField(
            model_name='userpoem',
            name='author',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
