from django.test import TestCase, Client
from django.contrib.auth.models import User
from app_poems.models import ReviewOfPubDomPoem, ReviewOfUserPoem
# from backend.settings import PUBLIC_DOMAIN_REVIEW_PATH, USER_REVIEW_PATH


class postReviewEndpointTest(TestCase):
    # Set up Django testing client with a logged-in user.  
    def setUp(self):
        self.c = Client()
        user_cred = {'username' : 'test_user',
                     'password' : 'test_pw',
                    }
        self.test_user = User.objects.create_user(**user_cred)
        self.c.post('/login/', user_cred)


    def testPostReviewOfUserPoem(self):
        review = {'poem_id' : 1, 
                  'user' : self.test_user,
                  'rating' : 3,
                  'blind_vote' : True,
                 }

        self.c.post('/reviews/user/', review)
        res = ReviewOfUserPoem.objects.filter(poem_id=1, user=self.test_user)[0]
        res = {'poem_id' : res.poem_id,
               'user' : res.user,
               'rating' : res.rating,
               'blind_vote' : res.blind_vote,
              }
        self.assertEqual(res, review)

    def testPostReviewOfPubDomPoem(self):
        review = {'poem_id' : 1, 
                  'user' : self.test_user,
                  'rating' : 3,
                  'blind_vote' : True,
                 }

        self.c.post('/reviews/lit/', review)
        res = ReviewOfPubDomPoem.objects.filter(poem_id=1, user=self.test_user)[0]
        res = {'poem_id' : res.poem_id,
               'user' : res.user,
               'rating' : res.rating,
               'blind_vote' : res.blind_vote,
              }
        self.assertEqual(res, review)

    def testUpdateAndUniquenessOfReviewOfUserPoem(self):
        # Post two reviews from the same user on the same poem.
        review = {'poem_id' : 1, 
                  'user' : self.test_user,
                  'rating' : 3,
                  'blind_vote' : True,
                 }

        new_review = {'poem_id' : 1, 
                      'user' : self.test_user,
                      'rating' : 2,
                      'blind_vote' : False
                     }

        self.c.post('/reviews/user/', review)
        self.c.post('/reviews/user/', new_review)

        # Check if results are unique and updated.
        res = ReviewOfUserPoem.objects.filter(poem_id=1, user=self.test_user)
        self.assertEqual(len(res), 1)           # Uniqueness check
        res = res[0]
        res = {'poem_id' : res.poem_id,
               'user' : res.user,
               'rating' : res.rating,
               'blind_vote' : res.blind_vote,
              }
        self.assertEqual(res, new_review)       # UPDATE check

    def testEndpoint400OnBadData(self):
        review = {'poem_id' : 1, 
                  'user' : self.test_user,
                  'rating' : 0,
                  'blind_vote' : True,
                 }

        resp = self.c.post('/reviews/user/', review)
        self.assertEqual(resp.status_code, 400)