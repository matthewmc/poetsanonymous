from app_poems.models import PubDomainPoem, UserPoem, Author
from django.test import TestCase, Client
from backend.settings import PUB_DOMAIN_PATH, USER_POEM_PATH
from django.contrib.auth.models import User


class getPoemJsonFromEndpointsTest(TestCase):
    def setUp(self):
        author = {'name' : 'author\'s name',
                  'info_url' : 'wikipedia.org/this_author',
                 }
        user = {'username' : 'test_user',
                'password' : 'test_password',
               }

        self.author = Author.objects.create(**author)
        self.user = User.objects.create(**user)

        self.pub_test_row = {'title' : 'Public Domain Poem Title',
                             'text' : 'Public Domain Poem Text', 
                             'author' : self.author,
                            }
        self.user_test_row = {'title' : 'User Poem Title',
                              'text' : 'User Poem Text',
                              'author' : self.user,
                              'date' : '3001-01-01',
                             }
        PubDomainPoem.objects.create(**self.pub_test_row)
        UserPoem.objects.create(**self.user_test_row)

        self.c = Client()
    
    def testPublicDomainPoemJsonEndpoint(self):
        pub_dom_poem_path = PUB_DOMAIN_PATH + '1/'
        res = self.c.get(pub_dom_poem_path).json()
        self.assertEqual(res['title'], self.pub_test_row['title'])
        self.assertEqual(res['text'], self.pub_test_row['text'])
        self.assertEqual(res['author'], self.pub_test_row['author'].name)

    def testUserPoemJsonEndpoint(self):
        user_poem_path = USER_POEM_PATH + '1/'
        res = self.c.get(user_poem_path).json()
        self.assertEqual(res['title'], self.user_test_row['title'])
        self.assertEqual(res['text'], self.user_test_row['text'])
        self.assertEqual(res['author'], self.user_test_row['author'].username)
        self.assertEqual(res['date'], self.user_test_row['date'])

    def testJsonEndpointWithInvalidId(self):
        pub_dom_poem_path = PUB_DOMAIN_PATH + '2/'
        res = self.c.get(pub_dom_poem_path)
        self.assertEqual(res.status_code, 404)