from django.test import TestCase, Client
from django.contrib.auth.models import User
from app_poems.models import PubDomainPoem, UserPoem, ReviewOfUserPoem, ReviewOfPubDomPoem, Author
from django.contrib.auth.models import User


class getPoemJsonFromEndpointsTest(TestCase):
    def setUp(self):
        self.pub_test_row = {'title' : 'Public Domain Poem Title',
                             'text' : 'Public Domain Poem Text', 
                             'author' : 'Public Domain Author',
                            }
        self.user_test_row = {'title' : 'User Poem Title',
                              'text' : 'User Poem Text',
                              'author' : "User Poem Author",
                             }
        self.c = Client()
        PubDomainPoem.objects.create(**self.pub_test_row)
        UserPoem.objects.create(**self.user_test_row)
        test_user = User.objects.create_user(username = 'test_user',
                                             password = 'test_password')
        review_1 = {'user' : test_user,
                    'rating' : 1,
                    'blind_vote' : True,
                    'poem_id' : 1,
                   }

        review_2 = {'user' : User.objects.create_user(username = 'test_user2',
                                                      password = 'test_password'),
                    'rating' : 4,
                    'blind_vote' : True,
                    'poem_id' : 1,
                   }

        review_3 = {'user' : User.objects.create_user(username = 'test_user3',
                                                      password = 'test_password'),
                    'rating' : 4,
                    'blind_vote' : False,
                    'poem_id' : 1,
                   }

        ReviewOfUserPoem.objects.create(**review_1)
        ReviewOfUserPoem.objects.create(**review_2)
        ReviewOfUserPoem.objects.create(**review_3)

    def testAvgRatings(self):
        res = self.c.post('/login/', {'username' : 'test_user3',
                                     'password' : 'test_password',
                                    })
        res = self.c.get('/poems/user/1/').json()
        self.assertEqual(res['blind_avg'], 2.5)
        self.assertEqual(res['total_avg'], 3)