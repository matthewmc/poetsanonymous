from json import dumps
from datetime import date
from random import choice

rows = []

def make_rows(model, num, fields):
    res = []
    for i in range(1, num + 1):
        row = {'model' : model,
               'pk' : i,
               'fields' : {key : '{0}-{1}-{2}'.format(model, key, str(i)) for key in fields}
              }
        if model == 'app_poems.PubDomainPoem' or model == 'app_poems.UserPoem':
            row['fields']['author'] = choice([1, 2, 3])
        if model == 'app_poems.UserPoem':       
            row['fields']['date'] = str(date.today())
        res.append(row)
    return res

rows += make_rows('app_poems.Author', 3, ['name', 'info_url'])
rows += make_rows('app_poems.PubDomainPoem', 10, ['title', 'author', 'text'])  
rows += make_rows('app_poems.UserPoem', 10, ['title', 'author', 'text'])

with open('poems.json', 'w+') as f:
    f.write(dumps(rows))