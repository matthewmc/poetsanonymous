# Objective: 
This project uses Django and React to create a website which can present a randomized stream of poetry that consists of a mix of public domain and 
user-sourced poetry.  Users can rate poems without knowing whether the author is another user or an author in the public domain.  Average ratings
are differentiated by whether the user voted knowing how the author is or whether they did it blindly.  Once a user votes cognizant of the author, 
their rating is permanently grouped with the other 

# Requirements:
* Python 3
* npm
Note: See requirements.txt and /frontend/package.json for full list of requirements.

# General Structure of the Project:
### Frontend:
Located in /frontend/

/frontend/src/ holds the unbundled CSS and Javascript for the React app.

/frontend/package.json contains relevant npm information for installing JS dependencies.

/frontend/webpack.config.js tells Webpack how to process the source code into a bundle that Django can serve.

### Backend:
Located in /backend/

/backend/backend/ holds the project (files of interest: settings.py, urls.py)

/backend/app_poems/ holds the application handling the poems (files of interest: models.py, forms.py, views.py)

# Getting Started:
1. Use pip and the requirements.txt file to install Python requirements.  Run "pip install -r requirements.txt"
        
2. Create a /backend/backend/credentials.py file to set your own Django SECRET_KEY.

3. Use npm from the command line to install JS requirements.
In /frontend/, run "npm install" 

4. In /backend/, run "python manage.py migrate" to build the database.  

5. In /backend/, run "python manage.py loaddata fixtures/simple_data/test_users.json fixtures/simple_data/poems.json" 
to load the database with basic entries.

# TO DO:
- Add user registration.
- Style the poem viewer React app.
- Fix the simple_data fixtures to work with recent database model changes.
- Write more test coverage.