'''
    Quick and dirty script to scrape https://www.poets.org for all poems in the public domain
    and write them to a json file.  Working as of 2018/3/19
    
'''

from bs4 import BeautifulSoup
import requests as req
import re
from time import sleep 
from json import dumps


# Returns a list of urls with page number appended.  
def get_page_links():
    pages = 136 # Num of pgs of pub dom. as of 2018/03
    # Url with params for public domain and pg
    url = ('https://www.poets.org/poetsorg/'
          'poems?field_poem_themes_tid=1456&page={0}')
    res = [url.format(i) for i in range(1, pages)]
    return res

# Takes url, returns BeautifulSoup obj
def get_page_soup(url):
    page = req.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    return soup

# Takes page soup and extracts list of poem page links.
def extract_links_from_page(soup):
    links = soup.find_all('a', {'href': re.compile('/poetsorg/poem/[a-z]+')})
    links = ['https://www.poets.org' + link['href'] for link in links]
    return links

# Takes poem page url and extracts title, author, text and returns in a dict.
def extract_poem_from_page(soup):
    title, author, text = None, None, None
    title = soup.find(id='page-title').text
    author = soup.find(itemprop="name").text
    text = soup.pre.text

    # Return JSON according to the Django fixture specifications and poems_app.models schema.
    return {
        "model" : "app_poems.PubDomainPoem",
        "pk" : None,
        "fields" : {
            "title" : title,
            "author" : author,
            "text" : text,
        }
    }

'''
Unused since poets.org lets you filter
# Checks text for signs that the poem is in the public domain.
def test_public_domain(soup):
    if len(soup(text="This poem is in the public domain.")) > 0:
        return True
    else:
        return False 
'''


if __name__ == "__main__":

    file_name = "../backend/fixtures/initial_literary_poems.json"

    # Get all urls for valid poems
    page_links = get_page_links()
    poem_links = []
    for link in page_links:
        new_links = extract_links_from_page(get_page_soup(link))
        poem_links.extend(new_links)

    with open(file_name, 'w+') as f:
        pk = 1
        res = []
        for link in poem_links:
            try:
                poem_soup = get_page_soup(link)
                poem_json = extract_poem_from_page(poem_soup)
                poem_json['pk'] = pk # Set key for db.
                pk += 1
                res.append(poem_json)
            except:
                pass
        f.write(dumps(res))
