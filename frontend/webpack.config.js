const path = require('path');

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, '../backend/static/src'),
    },
    module: {
        rules: [
        {
            test: /\.js$/,
            exclude: /node_modules/, 
            loader: 'babel-loader',
            query: {
                presets: ['react']      
            }
        },
        {
            test: /\.css$/,
            loader: 'css-loader' ,
        }]
    },
}

