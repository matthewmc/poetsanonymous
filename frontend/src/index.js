import React from 'react';
import ReactDOM from 'react-dom';
import Slider from 'rc-slider';
import StarRatingComponent from 'react-star-rating-component';
import '!style-loader!css-loader!rc-slider/assets/index.css'; 
import './index.css';

// Takes an inclusive ceiling and gives a random integer in the range of [1, excl_ceil].
function getRandInt(incl_ceil){
  return Math.floor(Math.random() * incl_ceil) + 1;
}

// Takes a probability, number between 1 and 100, that outcome is true, returns boolean probabilistically.
function rollProb(true_prob){
  if (getRandInt(100) <= true_prob) return true;
  else return false;
}

function Poem(props) {
  return (
    <div id="poem">
      {props.text}
    </div>
  );
}

function Author(props) {
  return (
    <div id="author">
      {props.author}
    </div>
  )
}

function Button(props){
  return (
    <button onClick={props.onClick}>
      {props.type}
    </button>
  )
}

// Icon to denote that user rating was made blind.
function Blind(props) {
  return (
    'Previously rated blind'
  )
}

// Check window.django for whether user is logged in.  Serve Log Out or Log In options based on state.
function LogInOut(props){
  if (window.django.is_logged_in == true){
    return (
      <div>
        <a href={'profiles/' + window.django.username}> {window.django.username} </a>
        <a href="logout/">Log Out</a>
      </div>
    )
  }
  else {
    return (
      <a href='login/'> Log In </a>
    )
  }
}

class Viewer extends React.Component{
  constructor(props) {
    super(props);
    this.state = {poem: "lorem ipsum",
                  num_of_user_poems: 10,
                  num_of_lit_poems: 10,
                  prob_of_user_poem: 50,
                  author: 'none',
                  lit_or_user: 'lit',
                  poem_id : 1,
                  revealed: false,
                  blind_avg: null,
                  total_avg: null,
                  user_rating: null,
                  rated_blind: true,
                 };
    this.changePoem = this.changePoem.bind(this);
    this.revealAuthor = this.revealAuthor.bind(this);
  };

  componentDidMount() {
    // Get the most current counts of the user-submitted poems and the open-sourced poems.
    let new_num_lit_poems = 10;
    let new_num_user_poems = 10;
    this.setState({num_of_lit_poems: new_num_lit_poems,
                   num_of_user_poems: new_num_user_poems,
                  });
  }
    
  // Function that rolls to determine category and id and then sets poem from endpoint in Viewer.     
  changePoem() {
    // Determine whether next poem should be user-sourced or 'lit'-erary (i.e. public domain)
    // and roll for poem id of the next poem.
    let lit_or_user = 'lit';
    let poem_id = 1;

    if (rollProb(this.state.prob_of_user_poem) === true){
      lit_or_user = 'user';
      poem_id = getRandInt(this.state.num_of_user_poems); 
    }
    else{
      lit_or_user = 'lit';
      poem_id = getRandInt(this.state.num_of_lit_poems); 
    }

    var req = new XMLHttpRequest();
    // Build endpoint to get the next poem.  
    req.open("GET", window.location.href + 'poems/' + lit_or_user + '/' + poem_id + '/');
    req.onload = () => {let text = JSON.parse(req.responseText);
                        // Update the Viewer Component's state.
                        this.setState({poem_id : poem_id,
                                       lit_or_user : lit_or_user,
                                       poem: text.text,
                                       author: text.author,
                                       revealed: false,
                                       total_avg: text.total_avg,
                                       blind_avg: text.blind_avg,
                                       user_rating: text.user_rating,
                                       rated_blind: text.rated_blind, 
                                      });       
                        };
    req.send();
  }

  // Changes whether the author is revealed.
  revealAuthor() {
    this.setState({revealed: 'true',})
  }

  render(){
    // Logic for the reveal author button that updates Viewer state and initiates another render() call.
    // If author not revealed, render button,; else render author.
    const revealAuthorButton = !this.state.revealed && this.state.rated_blind != false ? <Button type='Reveal'
                                                                                               onClick={this.revealAuthor} /> : this.state.author; 
    let prev_vote = '';
    if (this.state.rated_blind) prev_vote = <Blind />; 
    return (
      <div>
        <LogInOut /> 

        <Poem text={this.state.poem} />

        <Button type="NextPoem" onClick={this.changePoem} />
        {revealAuthorButton}

        Probability of user poem: {this.state.prob_of_user_poem}
        <Slider min={25} max={75} value={this.state.prob_of_user_poem} onChange={(value) => {this.setState({'prob_of_user_poem' : value})}} />
        
        Total avg:
        <StarRatingComponent name='total_avg' starCount={5} value={Math.round(this.state.total_avg)} editing={false} />
        
        Blind avg:
        <StarRatingComponent name='blind_avg' starCount={5} value={Math.round(this.state.blind_avg)} editing={false} />
        
        User rating:
        <StarRatingComponent name='user_rating' starCount={5} value={Math.round(this.state.user_rating)}
                             onStarClick={(value) => {this.setState({'user_rating': value});  // Update the user's rating.
                                                      // Build endpoint based on whether this is a user poem or public domain poem.
                                                      let endpoint = '/reviews/' + this.state.lit_or_user + '/';
                                                      let post = new XMLHttpRequest();
                                                      post.open('POST', endpoint);
                                                      post.setRequestHeader('X-CSRFTOKEN', window.django.csrf_token);
                                                      let formData = new FormData();
                                                      formData.append('rating', value);
                                                      formData.append('blind_vote', !this.state.revealed ? 'True' : 'False');
                                                      formData.append('poem_id', this.state.poem_id);
                                                      post.send(formData);
                                                     }
                                          } />
        {prev_vote}
      </div>
    )
  }
};


ReactDOM.render(
  <div>      
    <Viewer />
  </div>,
  document.getElementById('root')
);